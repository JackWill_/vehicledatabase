
public class Vehicle {
	
	String registration_number;
	int manufactured_year;
	
	void set_registration_number (String pno)
	
	{
		registration_number = pno;
	}
	
	void set_manufactured_year (int year)
	
	{
		manufactured_year =  year;
	}


	public static void main(String args[]) 
	{ 
		System.out.println("This is a Vehicle test branch");
		
		Vehicle v1 = new Vehicle();
		v1.set_manufactured_year(1999);
		v1.set_registration_number("GY11 S11");
		
		System.out.println("Vehicle Object Created");
	}
	
}